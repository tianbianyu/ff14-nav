# FF14-NAV

#### 介绍
FF14游戏的导航站，也可以拿来做自己的常用网址导航站

#### 软件架构
nuxt、vue2


#### 安装教程

1.  安装：npm install
2.  开发：npm run dev
3.  运行：npm run build
4.  打包：npm run generate
    打包在 dist 目录

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
